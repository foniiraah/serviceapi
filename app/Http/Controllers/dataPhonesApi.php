<?php

namespace App\Http\Controllers;

use App\DataPhone;
use Illuminate\Http\Request;

class dataPhonesApi extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAllData(){
        $data = DataPhone::all();
        return response($data);
    }
    public function getById($id){
        $data = DataPhone::where('id',$id)->get();
        return response ($data);
    }
    public function create(Request $request){
        $data = new DataPhone();
        $data->namephone = $request->input('namephone');
        $data->price = $request->input('price');
        $data->discount = $request->input('discount');
        $data->save();
    
        return response('Berhasil Menambah Data');
    }
    public function update(Request $request, $id){
        $data = DataPhone::where('id',$id)->first();
        $data->namephone = $request->input('namephone');
        $data->price = $request->input('price');
        $data->discount = $request->input('discount');
        $data->save();
    
        return response('Berhasil Merubah Data');
    }
    
    public function delete($id){
        $data = DataPhone::where('id',$id)->first();
        $data->delete();
    
        return response('Berhasil Menghapus Data');
    }
}